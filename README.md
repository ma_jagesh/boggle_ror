Boggle Game

## Requirements
	 Ruby 2.7.1p83
	 Rails 6.0.2.2
	 SQLite3 3.28.0 
	 Git 2.21.0

## Installation

Ruby: [https://www.ruby-lang.org/en/downloads/](https://www.ruby-lang.org/en/downloads/)
Rails: [https://guides.rubyonrails.org/getting_started.html#installing-rails](https://guides.rubyonrails.org/getting_started.html#installing-rails)
SQLite3: [https://www.sqlite.org/download.html](https://www.sqlite.org/download.html)
Git: [https://git-scm.com/downloads](https://git-scm.com/downloads)


## Git
Checkout the repository into your choice of directory.

   `git clone https://bitbucket.org/ma_jagesh/boggle_ror.git`

This will create a new folder named **boggle_ror**. Navigate inside the folder and open the terminal.

## Running the project

Now that the terminal is running and is pointing at the above created directory, lets install dependencies.

#### Start application 
`$ gem install bundler`

`$ bundle install`

`$ rails db:migrate`

`$ rails server -b 0.0.0.0 -p 8000 -e development`

This will install all the necessary dependencies and start the application in port 8000.

####  Execute test

`$ rails test`