class Board
  attr_accessor :player, :score, :count
  attr_reader :letters

  def initialize(player)
    @score = 0
    @count = 0
    @letters = Utils.generate(16)
    @player = player
  end
end