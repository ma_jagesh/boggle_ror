class Utils

  def self.does_player_exists_with_id(player_id)
    Player.exists?(id: player_id) ? yield(Player.find(player_id)) : yield(nil)
  end

  def self.does_player_exits_with_name(player_name)
    Player.exists?(name: player_name.upcase) ? yield(Player.find_by_name(player_name.upcase)) : yield(nil)
  end

  def self.generate(number)
    # charset = Array("A".."Z").shuffle
    # Array.new(number) { charset.sample }.join
    # chars = "IHKBFSDRJPBAQOKT" // for debug purpose
    chars = ""
    charset = "AAACCCBBBDDDFFFEEEGGGIIIHHHJJJLLLKKKMMMOOONNNPPPRRRQQQTTTSSSUUVVXXWWYYZ"

    number.times { chars += charset[rand(charset.length)].to_s}
    return chars.upcase
  end
end