Rails.application.routes.draw do
  namespace :api do
    resources :players

    post "/game", to: "games#new"
    post "/game/summary", to: "games#summary"
  end
end
