require "test_helper"

class Api::GamesControllerTest < ActionDispatch::IntegrationTest
  # test "can start a new game" do
  #   post "/api/game", params: {player_id: 1}
  #   assert_response :success
  # end


  test "can save game summary" do
    post "/api/game/summary", params: {score: 15, words_count: 2, player_id: 1}
    assert_response :created
  end
end
