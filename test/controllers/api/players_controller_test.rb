require "test_helper"

class Api::PlayersControllerTest < ActionDispatch::IntegrationTest
  test "index page" do
    get api_players_url
    assert_response :success
  end

  test "can create a player" do
    post "/api/players", params: {name: "james"}
    assert_response :created
  end

  test "can get player by id" do
    get "/api/players", params: {id: 1}
    assert_response :success
  end

  test "can delete player" do
    delete "/api/players/1"
    assert_response :no_content
  end

  test "can update player" do
    patch "/api/players/1"
    assert_response :accepted
  end

end
