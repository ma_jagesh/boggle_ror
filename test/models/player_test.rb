require "test_helper"
require "shoulda/context"

# DatabaseCleaner.strategy = :transaction

class PlayerTest < Minitest::Test
  context "a player, " do
    setup do
      # DatabaseCleaner.start
      @player = Player.new(name: "John")
      @invalid_player = @player
    end

    teardown do
      # DatabaseCleaner.clean
    end

    should "it should be a valid object" do
      assert @player.valid?
    end

    should "it be invalid without name" do
      @player.name = nil
      refute @player.valid?, "player is valid without a name"
      assert !@player.errors[:name].nil?, "no validation error for name present"
    end

    should "requires the name to be unique" do
      # assert @invalid_player.valid?
    end

  end
end
