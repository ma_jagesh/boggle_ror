require "test_helper"
require "minitest/autorun"
require "shoulda/context"

class GameTest < Minitest::Test
  context "a game" do
    setup do
      @player = Player.new(name: "John")
      @game = Game.new(score: 12, words_count: 3, player: @player)
    end

    should "it should be valid" do
      assert @game.valid?
    end

    should "it be invalid without score" do
      @game.score = nil
      refute @game.valid?, "game is valid without a score"
      assert !@game.errors[:score].nil?, "no validation error for score present"
    end

    should "it be invalid without words_count" do
      @game.words_count = nil
      refute @game.valid?, "game is valid without word_count"
      assert !@game.errors[:words_count].nil?, "no validation error for word_count present"
    end

    should "it be invalid without player" do
      @game.player = nil
      refute @game.valid?, "game is valid without player"
      assert !@game.errors[:player].nil?, "no validation error for player present"
    end
  end
end
