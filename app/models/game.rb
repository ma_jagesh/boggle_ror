class Game < ApplicationRecord
  belongs_to :player

  validates :score, presence: true
  validates :words_count, presence: true
end
