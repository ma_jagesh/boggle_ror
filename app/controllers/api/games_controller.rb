class Api::GamesController < Api::BaseController
  GAME_STARTED = ->(name) { "#{name} new game started." }
  THANK_YOU = "Thank you for playing."
  PLAYER_NOT_FOUND = "Player was not found."
  UNEXPECTED_ERROR = "An unexpected error occured."

  def player_id
    params[:player_id]
  end

  # POST /game
  def new
    player = Player.select("id, name").where(id: player_id).first
    unless player.nil?
      board = Board.new(player)
    end
    message = player.nil? ? PLAYER_NOT_FOUND : GAME_STARTED.call(player.name)
    status = player.nil? ? 404 : 201
    respond_with :api, json: Response.new(message, !player.nil?, player.nil? ? nil : board), status: status
  end

  # POST /game/summary
  def summary
    game = Game.create(score: params[:score], words_count: params[:words_count], player_id: params[:player_id])
    status = game.nil? ? 409 : 201
    message = game.nil? ? UNEXPECTED_ERROR : THANK_YOU
    respond_with :api, json: Response.new(message, true, game), status: status
  end

  private

  def game_params
    params.require(:game).permit(:player_id, :game)
  end
end
