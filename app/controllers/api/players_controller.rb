class Api::PlayersController < Api::BaseController
  ALL_PLAYERS = "List of all players."
  PLAYER_CREATED = ->(name) { "#{name} created." }
  PLAYER_DETAIL = "Detail of a player."
  PLAYER_EXISTS = ->(name) { "Player '#{name}' already exits." }
  PLAYER_NOT_FOUND = "Player was not found."
  PLAYER_UPDATED = "Player updated successfully."
  PLAYER_DELETE_SUCCESS = ->(name) { "Player '#{name}' deleted successfully." }

  def player_name
    params[:name]
  end

  def player_id
    params[:id]
  end

  # GET /players
  def index
    sql = "select players.id, players.name, (select count(*) from games where player_id = players.id) as gamesPlayed
          from players
          left join (
              select id,  player_id from games group by player_id
              ) as games
          on players.id = games.player_id
          order by players.name limit 10"
    player_records = ActiveRecord::Base.connection.execute(sql)
    # status = player_records.nil? ? 200 : 204
    respond_with :api, json: Response.new(ALL_PLAYERS, true, player_records), status: 200
  end

  # POST /players
  def create
    new_player = Player.new
    Utils.does_player_exits_with_name(player_name) do |player_obj|
      if player_obj.nil?
        new_player = Player.create(name: player_name.upcase)
      end
      status = !new_player.nil? ? 201 : 409
      message = player_obj.nil? ? PLAYER_CREATED.call(player_name) : PLAYER_EXISTS.call(player_name)
      respond_with :api, json: Response.new(message, player_obj.nil?, player_obj.nil? ? new_player : nil), status: status
    end
  end

  # GET /players/:id
  def show
    Utils.does_player_exists_with_id(player_id) do |player_obj|
      message = player_obj.nil? ? PLAYER_NOT_FOUND : PLAYER_DETAIL
      status = player_obj.nil? ? 200 : 404
      respond_with :api, json: Response.new(message, !player_obj.nil?, player_obj), status: status
    end
  end

  # DELETE /players/:id
  def destroy
    Utils.does_player_exists_with_id(player_id) do |player_obj|
      unless player_obj.nil?
        Player.destroy(player_id)
      end
      message = player_obj.nil? ? PLAYER_NOT_FOUND : PLAYER_DELETE_SUCCESS.call(player_obj.name)
      status = player_obj.nil? ? 204 : 404
      respond_with :api, json: Response.new(message, !player_obj.nil?, nil), status: status
    end
  end

  # PATCH /players/:id
  def update
    message = ""
    Utils.does_player_exists_with_id(player_id) do |player_obj|
      if player_obj.nil?
        message = PLAYER_NOT_FOUND
      else
        does_player_exits_with_name do |player|
          if player.nil?
            player_obj.update_attributes(name: player_name)
            message = PLAYER_UPDATED
          else
            message = PLAYER_EXISTS.call(player.name)
          end
        end
      end
      status = player_obj.nil? ? 202 : 404
      respond_with :api, json: Response.new(message, !player_obj.nil?, player_obj), status: status
    end
  end

  private

  def player_params
    params.require(:player).permit(:id, :name)
  end

end